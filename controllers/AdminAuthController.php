<?php

namespace app\controllers;

use app\models\Admin;
use app\models\AdminRefreshToken;
use sizeg\jwt\JwtHttpBearerAuth;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\AdminLoginForm;
use app\models\ContactForm;
use yii\web\UnauthorizedHttpException;

class AdminAuthController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
            'optional' => [
                'login',
                'refresh-token',
                'options'
            ],
        ];

        return $behaviors;
    }

    private function generateJwt(yii\web\User $admin) {
        $jwt = Yii::$app->jwt;
        $signer = $jwt->getSigner('HS256');
        $key = $jwt->getKey();
        $time = time();

        $jwtParams = Yii::$app->params['jwt'];

        return $jwt->getBuilder()
            ->issuedBy($jwtParams['issuer'])
            ->permittedFor($jwtParams['audience'])
            ->identifiedBy($jwtParams['id'], true)
            ->issuedAt($time)
            ->expiresAt($time + $jwtParams['expire'])
            ->withClaim('uid', $admin->id)
            ->getToken($signer, $key);
    }

    /**
     * @throws \yii\base\Exception
     * @throws \yii\web\ServerErrorHttpException
     */
    private function generateRefreshToken(yii\web\User $user, yii\web\User $impersonator = null)
    {
        $refreshToken = Yii::$app->security->generateRandomString(200);

        // TODO: Don't always regenerate - you could reuse existing one if user already has one with same IP and user agent
        $adminPreviosSession = \app\models\AdminRefreshToken::find()
            ->where(['admin_id' => $user->id, 'ip' => Yii::$app->request->userIP, 'user_agent' => Yii::$app->request->userAgent])->one();
        if ($adminPreviosSession)
        {
            $adminPreviosSession->token = $refreshToken;
            $adminRefreshToken = $adminPreviosSession;
        }
        else {
            $adminRefreshToken = new \app\models\AdminRefreshToken([
                'admin_id' => $user->id,
                'token' => $refreshToken,
                'ip' => Yii::$app->request->userIP,
                'user_agent' => Yii::$app->request->userAgent,
//                'created_at' => gmdate('Y-m-d H:i:s'),
            ]);
        }
        if (!$adminRefreshToken->save()) {
            throw new \yii\web\ServerErrorHttpException('Failed to save the refresh token: '. $adminRefreshToken->getErrorSummary(true));
        }

        // Send the refresh-token to the user in a HttpOnly cookie that Javascript can never read and that's limited by path
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'refresh-token',
            'value' => $refreshToken,
            'httpOnly' => true,
            'sameSite' => 'none',
            'secure' => true,
            'path' => '/auth/refresh-token',  //endpoint URI for renewing the JWT token using this refresh-token, or deleting refresh-token
        ]));

    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionLogin(): array
    {
        $model = new AdminLoginForm();
        if ($model->load(Yii::$app->request->getBodyParams()) && $model->login()) {
            $admin = Yii::$app->admin;

            $token = $this->generateJwt($admin);

            $this->generateRefreshToken($admin);

            return [
                'admin' => $admin->identity,
                'token' => (string) $token,
            ];
        }

        return $model->getFirstErrors();
    }


    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionRefreshToken() {
        $refreshToken = Yii::$app->request->cookies->getValue('refresh-token', false);
        if (!$refreshToken) {
            return new UnauthorizedHttpException('No refresh token found.');
        }

        $userRefreshToken = AdminRefreshToken::findOne(['token' => $refreshToken]);

        if (Yii::$app->request->getMethod() === 'POST') {
            // Getting new JWT after it has expired
            if (!$userRefreshToken) {
                return new UnauthorizedHttpException('The refresh token no longer exists.');
            }

            $admin = Admin::find()  //adapt this to your needs
            ->where(['id' => $userRefreshToken->admin_id])
//                ->andWhere(['not', ['usr_status' => 'inactive']])
                ->one();
            if (!$admin) {
                $userRefreshToken->delete();
                return new UnauthorizedHttpException('The user is inactive.');
            }

            $token = $this->generateJwt($admin);

            return [
                'status' => 'ok',
                'token' => (string) $token,
            ];

        }

        if (Yii::$app->request->getMethod() === 'DELETE') {
            // Logging out
            if ($userRefreshToken && !$userRefreshToken->delete()) {
                return new \yii\web\ServerErrorHttpException('Failed to delete the refresh token.');
            }

            return ['status' => 'ok'];
        }

        return new UnauthorizedHttpException('The user is inactive.');
    }

    /**
     * @return Response
     */
    public function actionData(): Response
    {
        return $this->asJson([
            'success' => true,
        ]);
    }
}
