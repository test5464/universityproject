<?php

namespace app\models;

use phpDocumentor\Reflection\Types\This;
use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "admins".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property AdminRefreshToken[] $adminRefreshToken
 */
class Admin extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admins';
    }

    public static function findByUsername($username)
    {
        return self::find()->where(['username' => $username])->one();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['username', 'password'], 'string', 'max' => 255],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[AdminRefreshTokens]].
     *
     * @return \yii\db\ActiveQuery|AdminRefreshTokenQuery
     */
    public function getAdminRefreshTokens()
    {
        return $this->hasMany(AdminRefreshToken::className(), ['admin_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return AdminQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AdminQuery(get_called_class());
    }

    public static function findIdentity($id)
    {
        return self::find()->where(['id' => $id])->one();
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        $token1 = Yii::$app->jwt->getParser()->parse((string) $token);
        return AdminRefreshToken::find()
            ->where(['admin_id' => (string) $token1->getClaim('uid') ])
//            ->andWhere(['<>', 'usr_status', 'inactive'])  //adapt this to your needs
            ->one()->admin;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    public function validateAuthKey($authKey)
    {
        $refresh_tokens = $this->getAdminRefreshTokens();

        return $refresh_tokens->token === $authKey;
    }
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }
}
