<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "promotions".
 *
 * @property int $id
 * @property int $percent
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Goods[] $goods
 */
class Promotion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promotions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['percent'], 'required'],
            [['percent'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'percent' => 'Percent',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Goods]].
     *
     * @return \yii\db\ActiveQuery|GoodsQuery
     */
    public function getGoods()
    {
        return $this->hasMany(Goods::className(), ['promotion_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return PromotionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PromotionQuery(get_called_class());
    }
}
