<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "admin_refresh_tokens".
 *
 * @property int $id
 * @property int|null $admin_id
 * @property string $token
 * @property string $ip
 * @property string $user_agent
 * @property string|null $created_at
 *
 * @property Admin $admin
 */
class AdminRefreshToken extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_refresh_tokens';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['admin_id'], 'integer'],
            [['token', 'ip', 'user_agent'], 'required'],
            [['created_at'], 'safe'],
            [['token', 'user_agent'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 50],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['admin_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'admin_id' => 'Admin ID',
            'token' => 'Token',
            'ip' => 'Ip',
            'user_agent' => 'User Agent',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Admin]].
     *
     * @return \yii\db\ActiveQuery|AdminQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }

    /**
     * {@inheritdoc}
     * @return AdminRefreshTokenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AdminRefreshTokenQuery(get_called_class());
    }
}
