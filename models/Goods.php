<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "goods".
 *
 * @property int $id
 * @property string $name
 * @property int $code
 * @property string $specifications
 * @property int|null $shop_id
 * @property int|null $subcategories_id
 * @property int|null $promotion_id
 * @property int|null $price
 * @property string|null $image
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property OrderGoods[] $orderGoods
 * @property Promotion $promotion
 * @property Shop $shop
 * @property Subcategory $subcategories
 * @property UserGoods[] $userGoods
 */
class Goods extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code', 'specifications'], 'required'],
            [['code', 'shop_id', 'subcategories_id', 'promotion_id', 'price'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'specifications', 'image'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['promotion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Promotion::className(), 'targetAttribute' => ['promotion_id' => 'id']],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shop::className(), 'targetAttribute' => ['shop_id' => 'id']],
            [['subcategories_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subcategory::className(), 'targetAttribute' => ['subcategories_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'specifications' => 'Specifications',
            'shop_id' => 'Shop ID',
            'subcategories_id' => 'Subcategories ID',
            'promotion_id' => 'Promotion ID',
            'price' => 'Price',
            'image' => 'Image',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[OrderGoods]].
     *
     * @return \yii\db\ActiveQuery|OrderGoodsQuery
     */
    public function getOrderGoods()
    {
        return $this->hasMany(OrderGoods::className(), ['goods_id' => 'id']);
    }

    /**
     * Gets query for [[Promotion]].
     *
     * @return \yii\db\ActiveQuery|PromotionQuery
     */
    public function getPromotion()
    {
        return $this->hasOne(Promotion::className(), ['id' => 'promotion_id']);
    }

    /**
     * Gets query for [[Shop]].
     *
     * @return \yii\db\ActiveQuery|ShopQuery
     */
    public function getShop()
    {
        return $this->hasOne(Shop::className(), ['id' => 'shop_id']);
    }

    /**
     * Gets query for [[Subcategories]].
     *
     * @return \yii\db\ActiveQuery|SubcategoryQuery
     */
    public function getSubcategories()
    {
        return $this->hasOne(Subcategory::className(), ['id' => 'subcategories_id']);
    }

    /**
     * Gets query for [[UserGoods]].
     *
     * @return \yii\db\ActiveQuery|UserGoodsQuery
     */
    public function getUserGoods()
    {
        return $this->hasMany(UserGoods::className(), ['goods_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return GoodsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GoodsQuery(get_called_class());
    }
}
