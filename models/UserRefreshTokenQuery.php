<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserRefreshToken]].
 *
 * @see UserRefreshToken
 */
class UserRefreshTokenQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UserRefreshToken[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserRefreshToken|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
