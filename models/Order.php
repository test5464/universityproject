<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property string $address
 * @property int $callback
 * @property int|null $payment_id
 * @property string|null $comment_to_courier
 * @property int $if_product_no
 * @property int $status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property CourierOrder[] $couriersOrders
 * @property OrderGoods[] $orderGoods
 * @property Payment $payment
 * @property Subscription[] $subscriptions
 * @property UserOrder[] $userOrders
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address', 'callback', 'if_product_no', 'status'], 'required'],
            [['callback', 'payment_id', 'if_product_no', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['address', 'comment_to_courier'], 'string', 'max' => 255],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::className(), 'targetAttribute' => ['payment_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Address',
            'callback' => 'Callback',
            'payment_id' => 'Payment ID',
            'comment_to_courier' => 'Comment To Courier',
            'if_product_no' => 'If Product No',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[CouriersOrders]].
     *
     * @return \yii\db\ActiveQuery|CourierOrderQuery
     */
    public function getCouriersOrders()
    {
        return $this->hasMany(CourierOrder::className(), ['order_id' => 'id']);
    }

    /**
     * Gets query for [[OrderGoods]].
     *
     * @return \yii\db\ActiveQuery|OrderGoodsQuery
     */
    public function getOrderGoods()
    {
        return $this->hasMany(OrderGoods::className(), ['order_id' => 'id']);
    }

    /**
     * Gets query for [[Payment]].
     *
     * @return \yii\db\ActiveQuery|PaymentQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * Gets query for [[Subscriptions]].
     *
     * @return \yii\db\ActiveQuery|SubscriptionQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscription::className(), ['order_id' => 'id']);
    }

    /**
     * Gets query for [[UserOrders]].
     *
     * @return \yii\db\ActiveQuery|UserOrderQuery
     */
    public function getUserOrders()
    {
        return $this->hasMany(UserOrder::className(), ['order_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }
}
