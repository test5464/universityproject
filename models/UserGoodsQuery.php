<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserGoods]].
 *
 * @see UserGoods
 */
class UserGoodsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UserGoods[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserGoods|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
