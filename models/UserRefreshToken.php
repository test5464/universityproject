<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users_refresh_tokens".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $token
 * @property string $ip
 * @property string $user_agent
 * @property string|null $created_at
 *
 * @property User $user
 */
class UserRefreshToken extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_refresh_tokens';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['token', 'ip', 'user_agent'], 'required'],
            [['created_at'], 'safe'],
            [['token', 'user_agent'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 50],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'token' => 'Token',
            'ip' => 'Ip',
            'user_agent' => 'User Agent',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return UserRefreshTokenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserRefreshTokenQuery(get_called_class());
    }
}
