<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subcategories".
 *
 * @property int $id
 * @property string $name
 * @property int|null $categories_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Category $categories
 * @property Goods[] $goods
 */
class Subcategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subcategories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['categories_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['categories_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categories_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'categories_id' => 'Categories ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Categories]].
     *
     * @return \yii\db\ActiveQuery|CategoryQuery
     */
    public function getCategories()
    {
        return $this->hasOne(Category::className(), ['id' => 'categories_id']);
    }

    /**
     * Gets query for [[Goods]].
     *
     * @return \yii\db\ActiveQuery|GoodsQuery
     */
    public function getGoods()
    {
        return $this->hasMany(Goods::className(), ['subcategories_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return SubcategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SubcategoryQuery(get_called_class());
    }
}
