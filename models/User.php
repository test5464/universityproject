<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $username
 * @property string $phone_number
 * @property string $email
 * @property string $password
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property UserRefreshToken[] $userRefreshToken
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    public static function findByUsername($username)
    {
        return self::find()->where(['username' => $username])->one();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'phone_number', 'email', 'password'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['username', 'email', 'password'], 'string', 'max' => 255],
            [['phone_number'], 'string', 'max' => 8],
            [['username'], 'unique'],
            [['phone_number'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'password' => 'Password',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[UsersRefreshTokens]].
     *
     * @return \yii\db\ActiveQuery|UserRefreshTokenQuery
     */
    public function getUserRefreshToken()
    {
        return $this->hasMany(UserRefreshToken::className(), ['user_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    public static function findIdentity($id)
    {
        return self::find()
            ->where(['id' => (string) $id])
//            ->andWhere(['<>', 'usr_status', 'inactive'])  //adapt this to your needs
            ->one();
    }

    public function login()
    {

    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        $token1 = Yii::$app->jwt->getParser()->parse((string) $token);
        return UserRefreshToken::find()
            ->where(['user_id' => (string) $token1->getClaim('uid') ])
//            ->andWhere(['<>', 'usr_status', 'inactive'])  //adapt this to your needs
            ->one()->user;

//        $userToken = UserRefreshToken::find()->where(['token' => $token])->one();
//
//        if ($userToken) {
//            return $userToken->getUser();
//        }
    }

    public function afterSave($isInsert, $changedOldAttributes) {
        // Purge the user tokens when the password is changed
        if (array_key_exists('usr_password', $changedOldAttributes)) {
            \app\models\UserRefreshToken::deleteAll(['user_id' => $this->id]);
        }
        parent::afterSave($isInsert, $changedOldAttributes);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }

    public function validatePassword($password): bool
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public function setPassword($password)
    {
        return Yii::$app->security->generatePasswordHash($password);
    }
}
