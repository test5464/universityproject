<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Courier]].
 *
 * @see Courier
 */
class CourierQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Courier[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Courier|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
