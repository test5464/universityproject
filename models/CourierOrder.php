<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "couriers_order".
 *
 * @property int $id
 * @property int|null $order_id
 * @property int|null $courier_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Courier $courier
 * @property Order $order
 */
class CourierOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'couriers_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'courier_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['courier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courier::className(), 'targetAttribute' => ['courier_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'courier_id' => 'Courier ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Courier]].
     *
     * @return \yii\db\ActiveQuery|CourierQuery
     */
    public function getCourier()
    {
        return $this->hasOne(Courier::className(), ['id' => 'courier_id']);
    }

    /**
     * Gets query for [[Order]].
     *
     * @return \yii\db\ActiveQuery|OrderQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * {@inheritdoc}
     * @return CourierOrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CourierOrderQuery(get_called_class());
    }
}
