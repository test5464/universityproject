<?php

use yii\db\Migration;

/**
 * Class m211109_221923_bug_fixing
 */
class m211109_221923_bug_fixing extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('admin_refresh_tokens','updated_at', $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP'));
        $this->addColumn('users_refresh_tokens','updated_at', $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('admin_refresh_tokens', 'updated_at');
        $this->dropColumn('users_refresh_tokens', 'updated_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211109_221923_bug_fixing cannot be reverted.\n";

        return false;
    }
    */
}
