<?php

use yii\db\Migration;

/**
 * Class m211029_232203_insert_users_admin
 */
class m211029_232203_insert_users_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%admins}}', [
            'id' => 1,
            'username' => 'admin',
            // TODO: Переделать
            'password' => '$2a$12$KkUF4hkp7fr1CC1l9sd2Ce9arrk5guFJ4ANQz6mcMuEeb6hAptSJm'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable("{{%admins}}");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211029_232203_insert_users_admin cannot be reverted.\n";

        return false;
    }
    */
}
