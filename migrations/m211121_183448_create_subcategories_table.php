<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%subcategories}}`.
 */
class m211121_183448_create_subcategories_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = 'subcategories';
        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'categories_id' => $this->integer(),
            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);
        $this->addForeignKey("fk_subcategories_categories", $tableName, 'categories_id', 'categories', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('subcategories');
    }
}
