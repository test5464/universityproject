<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_goods}}`.
 */
class m211101_120619_create_user_goods_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = 'user_goods';
        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'goods_id' => $this->integer(),
            'users_id' => $this->integer(),
            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);

        $this->addForeignKey("fk_user_goods_to_goods", $tableName, 'goods_id', 'goods', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("fk_user_goods_to_users", $tableName, 'users_id', 'users', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('%user_goods');
    }
}
