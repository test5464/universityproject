<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users_refresh_tokens}}`.
 */
class m211030_032120_create_users_refresh_tokens_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = '{{%users_refresh_tokens}}';
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'token' => $this->string()->notNull(),
            'ip' => $this->string(50)->notNull(),
            'user_agent' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')
        ], $tableOptions);

        $this->addForeignKey("fk_user_refresh_tokens", $tableName, 'user_id', 'users', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users_refresh_tokens}}');
    }
}
