<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_order}}`.
 */
class m211101_120646_create_user_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = 'user_order';
        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'users_id' => $this->integer(),
            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);

        $this->addForeignKey("fk_user_order_to_order", $tableName, 'order_id', 'order', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("fk_user_order_to_users", $tableName, 'users_id', 'users', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_order');
    }
}
