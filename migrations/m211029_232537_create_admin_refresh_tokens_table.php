<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m211029_232537_create_admin_refresh_tokens
 */
class m211029_232537_create_admin_refresh_tokens_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = 'admin_refresh_tokens';
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'admin_id' => $this->integer(),
            'token' => $this->string()->notNull(),
            'ip' => $this->string(50)->notNull(),
            'user_agent' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')
        ], $tableOptions);

        $this->addForeignKey("fk_admin_refresh_tokens", $tableName, 'admin_id', 'admins', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('admin_refresh_tokens');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211029_232537_create_admin_jwt cannot be reverted.\n";

        return false;
    }
    */
}
