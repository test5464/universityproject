<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%couriers_order}}`.
 */
class m211101_205538_create_couriers_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = 'couriers_order';
        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'couriers_id' => $this->integer(),
            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);

        $this->addForeignKey("fk_courier_order_to_order", $tableName, 'order_id', 'order', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("fk_courier_order_to_couriers", $tableName, 'couriers_id', 'couriers', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('couriers_order');
    }
}
