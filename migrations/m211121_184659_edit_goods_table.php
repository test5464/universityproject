<?php

use yii\db\Migration;

/**
 * Class m211121_184659_edit_goods_table
 */
class m211121_184659_edit_goods_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('goods','created_at');
        $this->dropColumn('goods','updated_at');
        $this->dropForeignKey('fk_goods_categories', 'goods');
        $this->dropColumn('goods', 'categories_id');
        $this->addColumn('goods','subcategories_id', $this->integer());
        $this->addForeignKey("fk_goods_subcategories", 'goods', 'subcategories_id', 'subcategories', 'id', 'CASCADE', 'CASCADE');
        $this->addColumn('goods','promotion_id', $this->integer());
        $this->addForeignKey("fk_goods_promotions", 'goods', 'promotion_id', 'promotions', 'id', 'CASCADE', 'CASCADE');
        $this->addColumn('goods', 'price', $this->integer());
        $this->addColumn('goods', 'image', $this->string());
        $this->addColumn('goods','created_at', $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'));
        $this->addColumn('goods','updated_at', $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP'));
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('goods','categories_id', $this->integer());
        $this->addForeignKey("fk_goods_categories", 'goods', 'categories_id', 'categories', 'id', 'CASCADE', 'CASCADE');
        $this->dropForeignKey('fk_goods_subcategories', 'goods');
        $this->dropColumn('goods', 'subcategories_id');
        $this->dropForeignKey('fk_goods_promotions', 'goods');
        $this->dropColumn('goods', 'promotion_id');
        $this->dropColumn('goods', 'price');
        $this->dropColumn('goods', 'image');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211121_184659_edit_goods_table cannot be reverted.\n";

        return false;
    }
    */
}
