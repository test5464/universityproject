<?php

use yii\db\Migration;

/**
 * Class m211121_185818_edit_orders_table
 */
class m211121_185818_edit_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('orders','created_at');
        $this->dropColumn('orders','updated_at');
        $this->addColumn('orders','comment_to_courier', $this->string());
        $this->addColumn('orders','if_product_no', $this->boolean()->notNull());
        $this->addColumn('orders','status', $this->boolean()->notNull());
        $this->addColumn('orders','created_at', $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'));
        $this->addColumn('orders','updated_at', $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders','comment_to_courier');
        $this->dropColumn('orders','if_product_no');
        $this->dropColumn('orders','status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211121_185818_edit_orders_table cannot be reverted.\n";

        return false;
    }
    */
}
