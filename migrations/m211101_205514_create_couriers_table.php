<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%couriers}}`.
 */
class m211101_205514_create_couriers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = 'couriers';
        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'users_id' => $this->integer(),
            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);

        $this->addForeignKey("fk_couriers_to_users", $tableName, 'users_id', 'users', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('courier');
    }
}
