<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_goods}}`.
 */
class m211101_120735_create_order_goods_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = 'order_goods';
        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'goods_id' => $this->integer(),
            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);

        $this->addForeignKey("fk_order_goods_to_order", $tableName, 'order_id', 'order', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("fk_order_goods_to_users", $tableName, 'goods_id', 'goods', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order_goods');
    }
}
