<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%subscriptions}}`.
 */
class m211121_191046_create_subscriptions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = 'subscriptions';
        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'user_id' => $this->integer(),
            'type' => $this->boolean()->notNull(),
            'time_of_delivery' => $this->timestamp(),
            'status' => $this->boolean()->notNull(),
            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);
        $this->addForeignKey("fk_subscriptions_to_order", $tableName, 'order_id', 'orders', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("fk_subscriptions_to_user", $tableName, 'user_id', 'users', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%subscriptions}}');
    }
}
