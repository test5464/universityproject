<?php

use yii\db\Migration;

/**
 * Class m211103_035600_bug_fixing
 */
class m211103_035600_bug_fixing extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameTable('payment', 'payments');
        $this->renameTable('order', 'orders');
        $this->renameTable('shop', 'shops');

        $this->renameColumn('user_order', 'users_id', 'user_id');
        $this->renameTable('user_order', 'user_orders');

        $this->renameColumn('user_goods', 'users_id', 'user_id');
        $this->renameColumn('couriers', 'users_id', 'user_id');
        $this->renameColumn('couriers_order', 'couriers_id', 'courier_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->renameTable('payments', 'payment');
        $this->renameTable('orders', 'order');
        $this->renameTable('shops', 'shop');

        $this->renameColumn('user_order', 'user_id', 'users_id');
        $this->renameTable('user_orders', 'user_order');

        $this->renameColumn('user_goods', 'user_id', 'users_id');
        $this->renameColumn('couriers', 'user_id', 'users_id');
        $this->renameColumn('couriers_order', 'courier_id', 'couriers_id');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211103_035600_bug_fixing cannot be reverted.\n";

        return false;
    }
    */
}
